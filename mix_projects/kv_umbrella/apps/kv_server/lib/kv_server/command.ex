defmodule KvServer.Command do
  @doc ~S"""
  Parses the given `line` into a command.

  ## Examples

      iex> KVServer.Command.parse("CREATE shopping\r\n")
      {:ok, {:create, "shopping"}}

  """
  def parse(line) do
    case String.split(line) do
        ["CREATE", bucket] -> {:ok, {:create, bucket}}
        ["GET", bucket, key] -> {:ok, {:grt, bucket, key}}
        ["PUT", bucket, key, value] -> {:ok, {:put, bucket, key, value}}
        ["DELETE", bucket, key] -> {:ok, {:delete, bucket, key}}
        _ -> {:error, :unknown_command}
    end
  end
end
