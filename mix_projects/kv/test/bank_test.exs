defmodule BankTest do
  use ExUnit.Case, async: true

  test "add amount in bank" do
    account = Bank.start_link(0)
    assert Bank.balance(account) == 0

    Bank.deposit(account, 10)
    assert Bank.balance(account) == 10
  end
end
