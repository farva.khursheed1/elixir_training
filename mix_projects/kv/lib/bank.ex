defmodule Bank do
  use Agent

  def start_link(balance) do
    Agent.start_link(fn -> balance end)
  end
  def deposit(account, amount) do
    Agent.update(account, &(&1 + amount))
  end
  def withdraw(account, amount) do
    Agent.update(account, &(&1 - amount))
  end
  def balance(account) do
    Agent.get(account, &(&1))
  end
end
