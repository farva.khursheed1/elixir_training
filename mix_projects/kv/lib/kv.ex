defmodule KV do
  use Application

  @impl true
  def start(_type, _args) do
    Supervisor.start_link(name: Supervisor)
  end
  @moduledoc """
  Documentation for `KV`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> KV.hello()
      :world

  """
  def hello do
  :world
  end
end
