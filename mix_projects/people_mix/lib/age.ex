defmodule People.Age do
  import Plug.Conn

  def init(options), do: options

  def call(%Plug.Conn{params: %{"birthday" => birthday}} = conn, _opts) do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "Your age is #{birthday}.")
  end
end
