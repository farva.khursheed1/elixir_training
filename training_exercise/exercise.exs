# Get Elixir to calculate the number of seconds in the day by multiplying the hours in a day by 60 twice. How many seconds are there in a day?
IO.puts(24 * 60 * 60 * 60)

# Calculate the average of these numbers: 4, 8, 15, 16, 23 and 42.
IO.puts((4+8+15+16+23+42)/6)

# If we store the number of seconds in a day using this code: seconds = 86400, calculate using that variable how many hours there are in 30 days.
seconds = 86400
IO.puts(seconds * 30)

# Create a variable called name, store a string in it and place the value of that variable in another string.
name = "Farva"
IO.puts("Hello #{name}")

#******************************************Function**************************************#

# Make a function which turns Fahrenheit temperatures into celsius.
fahrToCel = fn(fahrenheit) -> ((fahrenheit - 32) * (5/9)) end
IO.puts(fahrToCel.(770))

# Make a function which returns the number of seconds in the specified amount of days. For example, seconds.(2) should tell us how many seconds there are in 2 days.
dayToSec = fn(day) -> day * 86400 end
IO.puts(dayToSec.(2))

# Make a function which takes two maps with "age" keys in them and returns the average age.
avgAge = fn(age1, age2) -> (age1 + age2)/2 end
IO.puts(avgAge.(34,23))

#******************************************Input**************************************#

# Make a program that generates a very short story. Get it to take some input of a person, a place and an object -- using IO.gets/1 and combine all three into a little sentence, output with IO.puts/1.
name = IO.gets "What's your name? "
place = IO.gets "Where are you From? "
object = IO.gets "What food do you like the most? "

IO.puts "Hey #{String.trim(name)}! You are from #{String.trim(place)} and You like #{String.trim(object)} the most :-)"

#******************************************Pattern**************************************#

# Make a function that takes either a map containing a "name" and "age", or just a map containing "name". Change the output depending on if "age" is present. What happens if you switch the order of the function clauses? What can you learn from this?
data = fn
%{name: name,age: age} -> "Hello, #{name}! You are #{age} years old!"
%{name: name} -> "Hello, #{name}! Sorry I dont know your age"
%{} -> "Hello, Anonymous Stranger!"
end

IO.puts(data.(%{name: "Farva",age: 23}))
IO.puts(data.(%{name: "Farva"}))
IO.puts(data.(%{}))

#******************************************Lists**************************************#

# Use a combination of Enum.map/2 and String.replace/3 to replace all the e's in these words with another letter of your choosing: ["a", "very", "fine", "collection", "of", "words", "enunciated"]
string = ["a", "very", "fine", "collection", "of", "words", "enunciated"]
test = Enum.map(string, fn(str) -> String.replace(str,"e","a") end)
IO.puts(test)

# Use Enum.reduce/2 to multiply these numbers together: [5, 12, 9, 24, 9, 18]
list = [5, 12, 9, 24, 9, 18]
new = Enum.reduce(list, fn(score, product) -> score * product end)
IO.puts(new)

#******************************************Pipes**************************************#

# Use Enum.reduce/2 to multiply these numbers together: [5, 12, 9, 24, 9, 18]
list = [5, 12, 9, 24, 9, 18]
Enum.reduce(list, fn(score, product) -> score * product end)
|> IO.puts

#******************************************File System with conditions**************************************#

# read file and reverse the string with conditions
file_data = %{name: "file.txt"}
case Map.fetch(file_data, :name) do
  {:ok, name} ->
    case File.read(name) do
      {:ok, contents} ->
        contents
        |> String.split("\n", trim: true) \
        |> Enum.map(&String.reverse/1) \
        |> Enum.join("\n")
        |> IO.puts
      {:error, :enoent} ->
        IO.puts "No file exixts #{name}"
      {:error, _} ->
        IO.puts "Something bad happens please try again"
    end
  {:error, :enoent} ->
    IO.puts "No key called :name in file_data map"
end

# with ocndition
with {:ok, name} <- Map.fetch(file_data, :name),
    {:ok, contents} <- File.read(name) do
    contents
      |> String.split("\n", trim: true)
      |> Enum.map(&String.reverse/1)
      |> Enum.join("\n")
      |> IO.puts
else
    :error -> IO.puts "No key called :name in file_data map"
    {:error, :enoent} ->  IO.puts "No file exixts"
end

# write file
File.write("file1.txt", fixed_contents)
File.read("fixed-haiku.txt")
File.rename("file1.txt","files.txt")
File.rm("files.txt")

#******************************************Modules and Structs**************************************#

# get peron data through module
defmodule PersonData do
  defstruct [
    first_name: nil,
    last_name: nil,
    birthday: nil,
    location: "home"
  ]
  def name(%PersonData{first_name: first_name, last_name: last_name} = person) do
    "My name is #{first_name} #{last_name}"
  end

  def age(%PersonData{birthday: birthday} = person) do
    days =  Date.diff(Date.utc_today, birthday)
    days / 365.25
  end

  defp set_location(%Person{} = person, location) do
    IO.puts "#{person |> full_name}'s location is now #{location}"
    %{person | location: location}
  end

  def toggle_location(%Person{location: "away"} = person) do
    person |> set_location("home")
  end

  def toggle_location(%Person{location: "home"} = person) do
    person |> set_location("away")
  end
end

person = %{
  first_name: "Izzy",
  last_name: "Bell",
  birthday: ~D[1987-12-04],
  location: "home",
}

IO.puts(person |> PersonData.age)
IO.puts(person |> PersonData.name)
IO.puts(person |> PersonData.away)

#******************************************Processes**************************************#

# Take square of a list

defmodule Pararell do
  def pmap(collection, fun) do
    collection
    |> Enum.map(&spawn_process(&1, self, fun))
    |> Enum.map(&await/1)
  end

  defp spawn_process(item, parent, fun) do
    spawn_link fn ->
      send parent, {self, fun.(item)}
    end
  end

  defp await(pid) do
    receive do
      {^pid, value} ->
        value
      after 10000 ->
        "Message lost"
    end

  end
end

Pararell.pmap 1..10, &(&1 * &1)
|> IO.puts

#******************************************Protocols**************************************#

# Return data type

defprotocol Utility do
  @spec type(t) :: String.t()
  def type(value)
end

defimpl Utility, for: BitString do
  def type(_value), do: "string"
end

defimpl Utility, for: Integer do
  def type(_value), do: "integer"
end

Utility.type("farva") |> IO.puts
Utility.type(1234) |> IO.puts

#******************************************Protocols**************************************#

# Return count of data type

defmodule User do
  defstruct [:name, :age]
end

defprotocol Size do
  @doc "Calculate size"
  @fallback_to_any true
  def size(data)
end

defimpl Size, for: BitString do
  def size(string), do: byte_size(string)
end

defimpl Size, for: Map do
  def size(map), do: map_size(map)
end

defimpl Size, for: Tuple do
  def size(tuple), do: tuple_size(tuple)
end

defimpl Size, for: MapSet do
  set = %MapSet{} = MapSet.new
  def size(set), do: MapSet.size(set)
end

defimpl Size, for: User do
  def size(_user), do: 2
end

# defimpl Size, for: Any do
#   def size(_), do: 0
# end

Size.size("farva") |> IO.puts
Size.size(%{name: "farva"}) |> IO.puts
Size.size({:name, "farva"}) |> IO.puts
Size.size({}) |> IO.puts

#******************************************Comprehensions**************************************#

# Generators/Filters/:into

for n <- [1,2,3,4], rem(n,2) == 0, do: n * n

values = [good: 1,good: 2,bad: 3,good: 4,bad: 5]
for {:bad,n} <- values, do: n + n

for i <- [1,2,3], j <- [2,3], do: i * j

pixels = <<213, 45, 132, 64, 76, 32, 76, 0, 0, 234, 32, 15>>
for <<r::8, g::8, b::8 <- pixels>>, do: {r, g ,b}

for <<c <- " farva khursheed  ">>, c != ?\s, into: "", do: <<c>>

for {key, val} <- %{"class" => 12, "age" => 23}, into: %{}, do: {key, val + val}

#******************************************Try/catch**************************************#

try do
  Enum.each(-50..50, fn x -> if rem(x, 13) == 0, do: throw(x) end)
  IO.puts "nothing happens"
catch
  x -> IO.puts "Got #{x}"
end

try do
  IO.read(file, "hello")
  raise "Something bad happens"
after
  File.close(file)
end

#******************************************Debugging**************************************#

(1..10)
|> IO.inspect(label: "before")
|> Enum.map(fn x -> x * 2 end)
|> IO.inspect
|> Enum.sum
|> IO.inspect


__ENV__.file
|> String.split("/", trim: true)
|> List.last()
|> File.exists?()
|> dbg()

#******************************************Typespec**************************************#

defmodule Person do
  @typedoc """
  A 4 digit year
  """
  @type year :: integer
  @spec current_age(year) :: integer
  def current_age(year_of_birth), do:

end

defmodule LousyCalculator do
  @typedoc """
  Just a number followed by a string
  """
  @type number_with_remarks :: {number, String}
  @spec sum(number, number) :: {number, String.t}
  def sum(x,y), do: {x+y, "You need a calculator to do that? seriously?"}
end

#******************************************Behaviour**************************************#
defmodule Parser do
  @callback parse(String.t) :: {:ok, term} | {:error, String.t}
  @callback extension() :: [String.t]
end

defmodule JSONParser do
  @behaviour Parser

  @impl Parser
  def parse(str), do: {:ok, "some json" <> str}
  @impl Parser
  def extension(), do: ["JSON"]
end

defmodule XMLParser do
  @behaviour Parser

  def parse(str), do: {:ok, "some xml" <> str}
  def extension(), do: ["XML"]
end

defmodule BADParser do
  @behaviour Parser

  def parse, do: {:ok, "somthing bad happens"}
  def extension, do: ["BAD"]
end

#******************************************Agent**************************************#

iex(5)> {:ok,agent} = Agent.start_link(fn -> [] end)
{:ok, #PID<0.165.0>}
iex(6)> Agent.update(agent, fn _list -> 123  end)
:ok
iex(7)> Agent.update(agent, fn content -> %{a: content}  end)
:ok
iex(8)> Agent.update(agent, fn content -> ["farva" | content]  end)
:ok
iex(9)> Agent.update(agent, fn list -> [:hello | list]  end)
:ok
iex(10)> Agent.get(agent, fn content -> content end)
[:hello, "farva" | %{a: 123}]
iex(11)> Agent.update(agent, fn content -> [12 | content]  end)
:ok
iex(12)> Agent.get(agent, fn content -> content end)
[12, :hello, "farva" | %{a: 123}]
iex(13)>

#******************************************Quote/Unquote**************************************#

iex(7)> quote do: sum(1, 2, 3)
{:sum, [], [1, 2, 3]}
iex(8)> quote do: 1 * 2
{:*, [context: Elixir, imports: [{2, Kernel}]], [1, 2]}
iex(9)> quote do: %{a => 2}
{:%{}, [], [{{:a, [], Elixir}, 2}]}
iex(10)> quote do: :farva
:farva
iex(11)> quote do: x
{:x, [], Elixir}
iex(12)> x = 20
20
iex(13)> quote do: x + 2
{:+, [context: Elixir, imports: [{1, Kernel}, {2, Kernel}]],
 [{:x, [], Elixir}, 2]}
iex(14)> quote do: sum(1, 2+3, 4)
{:sum, [],
 [1, {:+, [context: Elixir, imports: [{1, Kernel}, {2, Kernel}]], [2, 3]}, 4]}
iex(15)> Macro.to_string(quote do: sum(1, 2))
"sum(1, 2)"
iex(16)> number = 12
12

#******************************************Macros**************************************#

defmodule Unless do
  def fun_unless(clause, do: expression) do
    if(!clause, do: expression)
  end
  defmacro macro_unless(clause, do: expression) do
    quote do
      if(!unquote(clause),do: unquote(expression))
    end
  end
end
