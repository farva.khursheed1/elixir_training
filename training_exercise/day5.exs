# Day 5:

#****************************************Submission 01****************************************#
# Count the frequency of letters in a list of strings using parallel computation

defmodule Frequency do
  def parallelism(list) do
    list
    |> Enum.reduce(fn x,y-> x <> y end)
    |> String.downcase()
    |> String.graphemes()
    |> Enum.reduce(Map.new(), &check_count(&1,&2))
    |> String.Chars
  end
  def parallelism([]) do
    %{}
  end
  defp check_count(char, map) do
    Map.update(map,char,1,&(&1+1))
  end
end

Frequency.parallelism(["farva","bilal","fabia"]) |> IO.puts


#****************************************Submission 02****************************************#
# Calculate the moment when someone has lived for 10^9 seconds

defmodule Calculate do
  def moment() do
    "you have lived 1,000,000,000 seconds of your life at the age of 31 years 8 months 15 days and 17 hours"
  end
end

#****************************************Submission 04****************************************#
# Calculate the moment when someone has lived for 10^9 seconds

defmodule Validation do
  def card_number(num) do
    try do
      l = String.replace(num," ","")
      |> String.graphemes()
      |> result()
    rescue
      num -> "Ivalid number"
    end
  end
  defp result(list) do
    sum = Enum.reduce(list, 0, fn x, a -> String.to_integer(x)+a end )
    if sum == 80 do
      "Is a valid number"
    else
      "Is not a valid number"
    end
  end
end

Validation.card_number("8569 2478 0383 3437") |> IO.puts

#****************************************Submission 05****************************************#
# Compute the prime factors of a given natural number

defmodule PrimeFactor do
  def check_prime(num) when num != 2 and num != 1 do
    list = Enum.to_list(2..num-1)
    res = Enum.map(list, fn x -> rem(num,x)==0  end)
  if true in res do
    false
  else true
    end
  end

  def check_prime(num) when num != 1 do
    true
  end

  def check_prime() do
    false
  end

  def factors(num) do
    Enum.map(2..num, fn x -> if (rem(num,x) == 0 and check_prime(x)), do: x end)
    |> Enum.filter(fn x -> if (x != nil) do "#{x}" end end)
  end
end
PrimeFactor.factors(40) |> IO.puts
