# Day 2:

#****************************************Submission 01****************************************#
# Count the occurrences of each word in that phrase

defmodule Word do
  def sentence(sentence) do
    word_list=sentence
    |>String.downcase()
    |>String.split()
    Enum.reduce(word_list, Map.new(), &count_words/2)
  end
  defp count_words(word,map) do
    Map.update(map, word, 1, &(&1 + 1))
  end
end


#****************************************Submission 02****************************************#
# Recite the lyrics to that beloved classic

 defmodule Recursion do
  def print_multiple_times(n) when n > 1 do
    IO.puts("#{n} bottles of beer on the wall, #{n} bottles of beer.")
    IO.puts("Take one down and pass it around, #{n-1} bottles of beer on the wall.")
    print_multiple_times(n - 1)
  end

  def print_multiple_times(n) when n == 1 do
    IO.puts("1 bottles of beer on the wall, 1 bottles of beer.")
    IO.puts("Take one down and pass it around, no bottles of beer on the wall.")
    IO.puts("no bottles of beer on the wall, no bottles of beer.")
    IO.puts("Take one down and pass it around, 99 bottles of beer on the wall.")
    :ok
  end
end

Recursion.print_multiple_times(99)


#****************************************Submission 02****************************************#
# Implement basic list operations.

# Length
defmodule Length do
  def length_list(list) do
    count = 0
    length_list(list,count)
  end
  def length_list([head | tail], count) do
    length_list(tail, count+1)
  end

  def length_list([], count) do
    count
  end
end

Length.length_list([1,2,3,4,5,6,6,7])
|> IO.puts

#Reduce
defmodule Reduce do
  def reduce_list([head | tail], enum) do
    reduce_list(tail, head + enum)
  end

  def reduce_list([], enum) do
    enum
  end
end

Reduce.reduce_list([1,2,3,4,5,6,6,7],0)
|> IO.puts

#Map
defmodule Map do
  def map_list(list) do
    l = length(list)
    IO.puts(hd(list) * 2)
    map_list(tl(list),l)
  end
  def map_list(list, l) when length(list) != 0 do
    IO.puts(hd(list) * 2)
    map_list(tl(list), l-1)
  end
  def map_list([], l) do
    :ok
  end
end

Map.map_list([1,2,3,4,5,6,6,7])
|> IO.puts
