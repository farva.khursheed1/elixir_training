# Day 4:

#****************************************Submission 01****************************************#
# Determine what the nth prime

defmodule Prime do
  def check_prime(num) when num != 2 and num != 1 do
    list = Enum.to_list(2..num-1)
    res = Enum.map(list, fn x -> rem(num,x)==0  end)
  if true in res do
    false
  else true
    end
  end

  def check_prime(num) when num != 1 do
    true
  end

  def check_prime() do
    false
  end

  def position(num) do
    if check_prime(num) do
      list = Enum.to_list(1..num)
      res = Enum.map(list, fn x -> check_prime(x) end)
      Enum.filter(res, fn x -> if (x != false) do x end end)
      "#{num} is #{length(num)}th prime number"
    end
  end
end

Prime.position(20) |> IO.puts

#****************************************Submission 02****************************************#
# Determine if a word or phrase is an isogram.

defmodule Phrase do
  def isogram(string) do
    list = String.graphemes(string)
    unique = Enum.uniq(list)
    if list == unique do
      "#{string} is Isogram,"
    else
      "#{string} is not Isogram"
    end
  end
end

Phrase.isogram("farva") |> IO.puts

#****************************************Submission 04****************************************#
# Take a nested list and return a single flattened list

defmodule NestedList do
  def flattened_list([head | tail]) do
    list = flattened_list(head) ++ flattened_list(tail)
    Enum.filter(list, fn x -> if (x != nil and x != []) do
      x end end)
  end
  def flattened_list([]) do
    []
  end
  def flattened_list(nil) do
    []
  end
end

NestedList.flattened_list([[1],[1,2,3,[4]]]) |> IO.puts

#****************************************Submission 05****************************************#
# Given a year, report if it is a leap year.

defmodule LeapYear do
  def check_year(year) do
    cond do
      rem(year, 400) == 0 or rem(year, 100) != 0 and rem(year, 4) == 0 -> "#{year} is a leap year"
      true -> "#{year} is not a leap year"
    end
  end
end

LeapYear.check_year(2000) |> IO.puts

#****************************************Submission 06****************************************#
# Given a word and a list of possible anagrams

defmodule Angrams do
  def get_list(word,list)do
    l=String.graphemes(word)
    list
    |> Enum.filter(&compare(&1,l))
  end
  def compare(ang,list) do
    ang=String.graphemes(ang)
    if ang -- list == [] do
      "#{ang}"
    end
  end
end

Angrams.get_list("google",["farva","oogleg","hello"]) |> IO.puts
