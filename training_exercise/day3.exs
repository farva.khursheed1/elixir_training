# Day 3:

#****************************************Submission 01****************************************#
# Create an implementation of the rotational cipher

defmodule Cipher do
  @cipher_alphabets %{0=>"a", 1=> "b", 2=> "c", 3=> "d", 4=> "e", 5=> "f", 6=> "g",7=> "h", 8=> "i", 9=> "j", 10=> "k", 11=> "l", 12=> "m", 13=> "n",14=> "o", 15=> "p", 16=> "q",17=> "r", 18=> "s", 19=> "t", 20=> "u", 21=> "v",22=> "w",23=> "x", 24=> "y",25=> "z"}
  @alphabets %{"a"=>0, "b"=> 1, "c"=> 2, "d"=> 3, "e"=> 4, "f"=> 5, "g"=> 6,"h"=> 7, "i"=> 8, "j"=> 9, "k"=> 10, "l"=> 11, "m"=> 12, "n"=> 13,"o"=> 14, "p"=> 15, "q"=> 16,"r"=> 17, "s"=> 18, "t"=> 19, "u"=> 20, "v"=> 21,"w"=> 22,"x"=> 23, "y"=> 24,"z"=> 25}
  def convert(text,shift) do
    text
    |> String.downcase()
    |> String.split()
    |> Enum.map(&word_char(&1,shift))
    |> Enum.map_join("&", fn (x) -> "#{x}" end)
    |> String.replace("&"," ")

  end
  def word_char(word,shift) do
    word
    |> String.graphemes()
    |> Enum.map(&char_rotate(&1,shift))
    |> to_string()
  end
  def char_rotate(char, shift) do
    cipher_alphabet=rem((@alphabets[char] + shift) , 26)
    @cipher_alphabets[cipher_alphabet]
  end
end

#****************************************Submission 02****************************************#
# Implement the keep and discard operation

defmodule KeepDiscard do
  def pred(collection, prediction) do
    cond do
      prediction == "Is the number even?" ->
        IO.puts("keep operation produce: ")
        keep(collection)
        IO.puts("keep operation produce: ")
        discard(collection)
      true ->
      IO.puts "provide valid predicate please"
    end
  end

  defp keep(list) do
    Enum.map(list, fn x -> if rem(x,2)==0 do
      IO.puts x
    end   end)
  end

  defp discard(list) do
    Enum.map(list, fn x -> if rem(x,2)!=0 do
      IO.puts x
    end   end)
  end
end

KeepDiscard.pred([1,2,3,4,5],"Is the number ev")


#****************************************Submission 03****************************************#
# Given an age in seconds, calculate how old someone would be on

defmodule Age do
  def planetage(sec, planet) do
    cond do
      planet == "Earth" ->
        IO.puts "#{sec / 31556952 |> Float.ceil(2)} Earth year's old"
      planet == "Mercury" ->
        IO.puts "#{sec / 76005.4381992 |> Float.ceil(2)} Mercury year's old"
      planet == "Venus" ->
        IO.puts "#{sec / 48976.5305218 |> Float.ceil(2)} Venus year's old"
      planet == "Mars" ->
        IO.puts "#{sec / 593540.326901 |> Float.ceil(2)} Mars year's old"
      planet == "Jupiter" ->
        IO.puts "#{sec / 3743556.59124 |> Float.ceil(2)} Jupiter year's old"
      planet == "Saturn" ->
        IO.puts "#{sec / 9292923.62885 |> Float.ceil(2)} Saturn year's old"
      planet == "Uranus" ->
        IO.puts "#{sec / 26513700.1933 |> Float.ceil(2)} Uranus year's old"
      planet == "Neptune" ->
        IO.puts "#{sec / 52004185.6003 |> Float.ceil(2)} Neptune year's old"
    end
  end
end

Age.planetage(1000000000,"Neptune")


#****************************************Submission 04****************************************#
# Given a string of digits, output all the contiguous substrings

defmodule CharList do
  def contiguous(string) do
    length = String.length(string)
    string_to_substring(length,string)
  end
  def string_to_substring(length,string) do
    if length>1 do
      string
      |> String.graphemes()
      |> Enum.chunk_every(length, 1, :discard)
      |> Enum.map(&to_string/1)
      |> IO.puts
      string_to_substring(length-1,string)
    end
  end
end

CharList.contiguous("1234")

#****************************************Submission 05****************************************#
# The contents of which depend on the number's factors.

defmodule Factor do
  def digit(num) do
    cond do
      rem(num, 3)==0 and rem(num, 5)==0 and rem(num, 7)==0 ->
        "PlingPlangPlong"
      rem(num, 3)==0 and rem(num, 5)==0 ->
        "PlingPlang"
      rem(num, 3)==0 and rem(num, 7)==0 ->
        "PlingPlong"
      rem(num, 5)==0 and rem(num, 7)==0 ->
        "PlangPlong"
      rem(num, 3)==0 ->
        "Pling"
      rem(num, 5)==0 ->
        "Plang"
      rem(num, 7)==0 ->
        "Plong"
      true ->
        "#{num}"
    end
  end
end

Factor.digit(34) |> IO.puts

#****************************************Submission 06****************************************#
# Given two lists determine if the first list is contained within the second list

defmodule Lists do
  def check(list1, list2) do
    cond do
      list1 == list2 -> "A is equal to B"
      list1 -- list2 == [] -> "A is a sublist of B"
      list1 <= list2 -> "A is a superlist of B"
      true -> " A is not a superlist of, sublist of or equal to B"
    end
  end
end
Lists.check([3,4],[2,3,4]) |> IO.puts


#****************************************Submission 07****************************************#
# Given a word, compute the scrabble score for that word.

defmodule Score do
  @score_map %{"A"=>1,"E"=>1,"I"=>1,"O"=>1,"L"=>1,"N"=>1,"R"=>1,"S"=>1,"T"=>1,"D"=>2,"G"=>2,"B"=>3,"C"=>3,"M"=>3,"P"=>3,"F"=>4,"H"=>4,"V"=>4,"W"=>4,"Y"=>4,"K"=>5,"J"=>8,"X"=>8,"Q"=>10,"Z"=>10,"U"=>1}
  def word(string) do
    string
    |> String.trim()
    |> String.upcase
    |> String.graphemes
    |> Enum.map(&get_score_map(&1))
    |> Enum.sum()
  end
  defp get_score_map(char) do
    Map.get(@score_map,char)
  end
end

Score.word(" \t\n") |>IO.puts
