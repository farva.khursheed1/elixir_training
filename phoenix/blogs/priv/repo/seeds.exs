# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Blogs.Repo.insert!(%Blogs.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

#seeds some categories
for title <- ["Programming", "Painting", "Gardening", "Books", "Brands", "Cricket"] do
    {:ok, _} = Blogs.Content.create_category(%{title: title})
end
