defmodule Blogs.Repo.Migrations.CommentsDropIndexColumn do
  use Ecto.Migration

  def up do
    drop unique_index(:comments, [:post_id, :user_id])
  end

  def down do
    create unique_index(:comments, [:post_id, :user_id])
  end
end
