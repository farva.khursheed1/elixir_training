defmodule Blogs.Repo.Migrations.CreateCategories do
  use Ecto.Migration

  def up do
    create table(:categories) do
      add :title, :string

      timestamps()
    end

    create unique_index(:categories, [:title])
  end

  def down do
    drop unique_index(:categories, [:title])
    drop table(:categories)
  end

end
