defmodule Blogs.Repo.Migrations.CreatePosts do
  use Ecto.Migration

  def up do
    create table(:posts) do
      add :title, :string, null: false
      add :body, :text
      add :views, :integer, default: 0, null: false
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end
    create index(:posts, [:user_id])
  end

  def down do
    drop index(:posts, [:user_id])
    drop table(:posts)
  end
end
