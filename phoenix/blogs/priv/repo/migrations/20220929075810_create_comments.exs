defmodule Blogs.Repo.Migrations.CreateComments do
  use Ecto.Migration

  def up do
    create table(:comments) do
      add :body, :text, null: false
      add :post_id, references(:posts, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create index(:comments, [:post_id])
    create index(:comments, [:user_id])
    create unique_index(:comments, [:post_id, :user_id])
  end

  def down do
    drop index(:comments, [:post_id])
    drop index(:comments, [:user_id])
    drop unique_index(:comments, [:post_id, :user_id])
    drop table(:comments)
  end
end
