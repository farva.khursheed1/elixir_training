defmodule Blogs.Repo.Migrations.PostsAddPhotoColumn do
  use Ecto.Migration

  def up do
    alter table(:posts) do
      add :photo, :string
    end
  end

  def down do
    alter table(:posts) do
      drop :photo, :string
    end
  end
end
