defmodule Blogs.Repo.Migrations.CreateProductCategories do
  use Ecto.Migration

  def up do
    create table(:post_categories, primary_key: false) do
      add :post_id, references(:posts, on_delete: :delete_all)
      add :category_id, references(:categories, on_delete: :delete_all)
    end

    create index(:post_categories, [:post_id])
    create index(:post_categories, [:category_id])
    create unique_index(:post_categories, [:post_id, :category_id])
  end

  def down do
    drop index(:post_categories, [:post_id])
    drop index(:post_categories, [:category_id])
    drop unique_index(:post_categories, [:post_id, :category_id])
    drop table(:post_categories)
  end
end
