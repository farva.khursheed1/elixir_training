defmodule Blogs.Content.Post do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset
  alias Blogs.Content.Comment
  alias Blogs.Content.Category
  alias Blogs.Accounts.User

  schema "posts" do
    field :body, :string
    field :photo, Blogs.Photo.Type
    field :title, :string
    field :views, :integer
    belongs_to :user, User
    has_many :comments, Comment
    many_to_many :categories, Category, join_through: "post_categories", on_replace: :delete

    timestamps()
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :body, :views, :user_id, :photo])
    |> cast_attachments(attrs, [:photo])
    |> validate_required([:title, :body, :views, :user_id, :photo])
  end
end
