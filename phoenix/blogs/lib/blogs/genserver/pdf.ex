defmodule Blogs.GenServer.Pdf do
  use GenServer
  require Logger

  # Client API
  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, false, opts ++ [name: __MODULE__])
  end

  def save_pdf(html) do
    GenServer.call(__MODULE__, {:save_pdf, html})
  end

  # Server API
  @impl true
  def init(state) do
    if is_boolean(state) do
      {:ok, state}
    else
      {:stop, "State is not boolean"}
    end
  end

  @impl true
  def handle_call({:save_pdf, html}, _from, _state) do
    {:ok, filename} = reply = download_file(html)
    new_state = true
    Logger.info("PDF download Successfully")
    {:reply, filename, new_state}
  end

  @impl true
  def handle_info(msg, state) do
    Logger.debug("Unexpected message in Pdf Download: #{inspect(msg)}")
    {:noreply, state}
  end

  def download_file(html) do
    {:ok, filename} = PdfGenerator.generate(html, page_size: "A4", no_sandbox: true, shell_params: ["--dpi", "300","--print-media-type"])
  end

end
