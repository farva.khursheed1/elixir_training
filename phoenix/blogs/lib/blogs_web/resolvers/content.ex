defmodule BlogsWeb.Resolvers.Content do
  def list_posts(_parent, _args, _resolution) do
    {:ok, Blogs.Content.list_posts()}
  end

  # def create_post(_parent, args, _context) do
  #   Blogs.Content.add_post(args)
  # end
end
