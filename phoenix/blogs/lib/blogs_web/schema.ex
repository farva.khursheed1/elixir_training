defmodule BlogsWeb.Schema do
  use Absinthe.Schema

  import_types Absinthe.Type.Custom
  import_types BlogsWeb.Schema.ContentTypes

  alias BlogsWeb.Resolvers

  query do
    field :posts, list_of(:post) do
      resolve &Resolvers.Content.list_posts/3
    end
  end

  # mutation do
  #   field :create_post, type: :post do
  #     arg :title, non_null(:string)
  #     arg :body, non_null(:string)
  #     arg :views, non_null(:integer)

  #     resolve &Resolvers.Content.create_post/3
  #   end
  # end
end
