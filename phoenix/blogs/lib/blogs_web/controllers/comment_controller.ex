defmodule BlogsWeb.CommentController do
  use BlogsWeb, :controller

  alias Blogs.Content
  alias Blogs.Content.Comment
  alias Blogs.Content.Post

  def create(conn, %{"comment" => comment_params, "post_id" => post_id}) do
    post = Content.get_post!(post_id)
    user_id = conn.assigns.current_user.id
    changeset = Ecto.build_assoc(post, :comments, user_id: user_id)
    case Content.create_comment(changeset, comment_params) do
      {:ok, _comment} ->
        conn
        |> put_flash(:info, "Comment added :)")
        |> redirect(to: Routes.post_path(conn, :show, post))
      {:error, _error} ->
        conn
        |> put_flash(:error, "Comment not added :(")
        |> redirect(to: Routes.post_path(conn, :show, post))
    end
  end

  def edit(conn, %{"id" => id, "post_id" => post_id}) do
    comment = Content.get_comment!(id)
    post = Content.get_post!(post_id)
    changeset = Content.change_comment(comment)
    render(conn, "edit.html", comment: comment, post: post, changeset: changeset)
  end

  def update(conn, %{"id" => id, "comment" => comment_params, "post_id" => post_id}) do
    comment = Content.get_comment!(id)

    case Content.update_comment(comment, comment_params) do
      {:ok, comment} ->
        conn
        |> put_flash(:info, "comment updated successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post_id))

        {:error, _} ->
          conn
          |> put_flash(:info, "Failed to update comment!")
          |> redirect(to: Routes.post_path(conn, :show, post_id))
    end
  end

  def delete(conn, %{"id" => id, "post_id" => post_id}) do
    comment = Content.get_comment!(id)
    {:ok, _comment} = Content.delete_comment(comment)

    conn
    |> put_flash(:info, "comment deleted successfully.")
    |> redirect(to: Routes.post_path(conn, :show, post_id))
  end
end
