defmodule BlogsWeb.PostController do
  use BlogsWeb, :controller

  alias Blogs.Content
  alias Blogs.Content.Post
  alias Blogs.Content.Comment
  alias Blogs.GenServer.Pdf

  def index(conn, _params) do
    posts = Content.list_posts()
    render(conn, "index.html", posts: posts)
  end

  def login_index(conn, _params) do
    id = conn.assigns.current_user.id
    posts = Content.login_posts(id)
    render(conn, "login_index.html", posts: posts)
  end

  def new(conn, _params) do
    changeset = Content.change_post(%Post{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"post" => post_params}) do
    current_user = conn.assigns.current_user
    changeset = Ecto.build_assoc(current_user, :posts, post_params)
    case Content.create_post(changeset, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post created successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    post =
      id
      |> Content.get_post!()
      |> Content.inc_page_views()
    comment_changeset = Content.change_comment(%Comment{})
    render(conn, "show.html", post: post, comment_changeset: comment_changeset)
  end

  def edit(conn, %{"id" => id}) do
    post = Content.get_post!(id)
    changeset = Content.change_post(post)
    render(conn, "edit.html", post: post, changeset: changeset)
  end

  def update(conn, %{"id" => id, "post" => post_params}) do
    post = Content.get_post!(id)

    case Content.update_post(post, post_params) do
      {:ok, post} ->
        conn
        |> put_flash(:info, "Post updated successfully.")
        |> redirect(to: Routes.post_path(conn, :show, post))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", post: post, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    post = Content.get_post!(id)
    {:ok, _post} = Content.delete_post(post)

    conn
    |> put_flash(:info, "Post deleted successfully.")
    |> redirect(to: Routes.post_path(conn, :index))
  end

  # pdf implementation without genserver
  # def pdf(conn, %{"id" => id}) do
  #   post =
  #     id
  #     |> Content.get_post!()
  #     |> Content.inc_page_views()
  #   html = Phoenix.View.render_to_string(BlogsWeb.PostView, "pdf.html", post: post)
  #   case PdfGenerator.generate(html, page_size: "A4", shell_params: ["--dpi", "300"]) do
  #     {:ok, filename} ->
  #       :ok = File.rename(filename, "./priv/static/post_pdfs/post_#{id}_#{DateTime.utc_now()}.pdf")
  #       conn
  #       |> put_flash(:info, "PDF saved successfully")
  #       |> redirect(to: Routes.post_path(conn, :show, post))

  #     {:error, _changeset} ->
  #       conn
  #       |> put_flash(:error, "PDF Failed")
  #       |> redirect(to: Routes.post_path(conn, :show, post))
  #   end
  # end

  # pdf implementation with genserver
  def pdf(conn, %{"id" => id}) do
    post =
      id
      |> Content.get_post!()
      |> Content.inc_page_views()
    html = Phoenix.View.render_to_string(BlogsWeb.PostView, "pdf.html", post: post)
    case Pdf.save_pdf(html) do
      filename ->
        conn
        |> put_resp_content_type("application/pdf", "utf-8")
        |> put_resp_header("content-disposition", "attachment; filename=\"post_#{id}_#{DateTime.utc_now()}.pdf\"")
        |> put_flash(:info, "PDF saved successfully")
        |> send_file(200, filename)
      _ ->
        conn
        |> put_flash(:error, "PDF Failed")
        |> redirect(to: Routes.post_path(conn, :show, post))
    end
  end

end
