defmodule BlogsWeb.PostView do
  use BlogsWeb, :view

  def category_select(f, changeset) do
    existing_ids = changeset |> Ecto.Changeset.get_change(:categories, []) |> Enum.map(& &1.data.id)

    category_opts =
      for cat <- Blogs.Content.list_categories(),
          do: [key: cat.title, value: cat.id, selected: cat.id in existing_ids]

    multiple_select(f, :category_ids, category_opts)
  end

  def get_comments_count(post_id) do
    Blogs.Content.get_number_of_comments(post_id)
  end
end
