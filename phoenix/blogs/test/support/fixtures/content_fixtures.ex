defmodule Blogs.ContentFixtures do
  alias Blogs.Content.Post
  alias Blogs.Accounts.User
  alias Blogs.Content.Comment
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Blogs.Content` context.
  """

  @doc """
  Generate a post.
  """
  def post_fixture(%User{} = user, attrs \\ %{}) do
    post = %Post{user_id: user.id}
    changeset = Enum.into(attrs, %{
      body: "some body",
      title: "some title",
      views: 42,
      photo: "photo.png"
    })
    {:ok, post} = Blogs.Content.create_post(post, changeset)
    post
  end

  @doc """
  Generate a unique category title.
  """
  def unique_category_title, do: "some title#{System.unique_integer([:positive])}"

  @doc """
  Generate a category.
  """
  def category_fixture(attrs \\ %{}) do
    {:ok, category} =
      attrs
      |> Enum.into(%{
        title: unique_category_title()
      })
      |> Blogs.Content.create_category()

    category
  end

  @doc """
  Generate a comment.
  """
  def comment_fixture(%Post{} = post,%User{} = user, attrs \\ %{}) do
    comment = %Comment{post_id: post.id, user_id: user.id}
    changeset = attrs
    |> Enum.into(%{
      body: "some body"
    })

    {:ok, comment} = Blogs.Content.create_comment(comment, changeset)

    comment
  end
end
