defmodule School.Repo.Migrations.CreateStudents do
  use Ecto.Migration

  def up do
    create table(:students) do
      add :name, :string
      add :email, :string
      add :bio, :string
      add :number_of_pets, :integer

      timestamps()
    end
  end

  def down do
    drop table(:students)
  end
end
