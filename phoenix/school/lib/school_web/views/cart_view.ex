defmodule SchoolWeb.CartView do
    use SchoolWeb, :view
  
    alias School.ShoppingCart
  
    def currency_to_str(%Decimal{} = val), do: "$#{Decimal.round(val, 2)}"
  end