defmodule ChattApp.TalkFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ChattApp.Talk` context.
  """

  @doc """
  Generate a room.
  """
  def room_fixture(attrs \\ %{}) do
    {:ok, room} =
      attrs
      |> Enum.into(%{
        description: "some description",
        title: "some title",
        topic: "some topic"
      })
      |> ChattApp.Talk.create_room()

    room
  end
end
