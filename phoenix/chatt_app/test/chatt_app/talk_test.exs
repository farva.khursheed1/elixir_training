defmodule ChattApp.TalkTest do
  use ChattApp.DataCase

  alias ChattApp.Talk

  describe "rooms" do
    alias ChattApp.Talk.Room

    import ChattApp.TalkFixtures

    @invalid_attrs %{description: nil, title: nil, topic: nil}

    test "list_rooms/0 returns all rooms" do
      room = room_fixture()
      assert Talk.list_rooms() == [room]
    end

    test "get_room!/1 returns the room with given id" do
      room = room_fixture()
      assert Talk.get_room!(room.id) == room
    end

    test "create_room/1 with valid data creates a room" do
      valid_attrs = %{description: "some description", title: "some title", topic: "some topic"}

      assert {:ok, %Room{} = room} = Talk.create_room(valid_attrs)
      assert room.description == "some description"
      assert room.title == "some title"
      assert room.topic == "some topic"
    end

    test "create_room/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Talk.create_room(@invalid_attrs)
    end

    test "update_room/2 with valid data updates the room" do
      room = room_fixture()
      update_attrs = %{description: "some updated description", title: "some updated title", topic: "some updated topic"}

      assert {:ok, %Room{} = room} = Talk.update_room(room, update_attrs)
      assert room.description == "some updated description"
      assert room.title == "some updated title"
      assert room.topic == "some updated topic"
    end

    test "update_room/2 with invalid data returns error changeset" do
      room = room_fixture()
      assert {:error, %Ecto.Changeset{}} = Talk.update_room(room, @invalid_attrs)
      assert room == Talk.get_room!(room.id)
    end

    test "delete_room/1 deletes the room" do
      room = room_fixture()
      assert {:ok, %Room{}} = Talk.delete_room(room)
      assert_raise Ecto.NoResultsError, fn -> Talk.get_room!(room.id) end
    end

    test "change_room/1 returns a room changeset" do
      room = room_fixture()
      assert %Ecto.Changeset{} = Talk.change_room(room)
    end
  end
end
