defmodule ChattApp.Talk.Room do
  use Ecto.Schema
  import Ecto.Changeset
  alias ChattApp.Talk.Room
  alias ChattApp.Accounts.User

  schema "rooms" do
    field :description, :string
    field :title, :string
    field :topic, :string
    belongs_to :user, User
    has_many :messages, ChattApp.Talk.Message

    timestamps()
  end

  @doc false
  def changeset(%Room{} = room, attrs) do
    room
    |> cast(attrs, [:title, :topic, :description, :user_id])
    |> validate_required([:title])
    |> unique_constraint(:title)
    |> validate_length(:title, min: 5, max: 30)
    |> validate_length(:topic, min: 5, max: 120)
  end
end
