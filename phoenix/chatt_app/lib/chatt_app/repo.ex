defmodule ChattApp.Repo do
  use Ecto.Repo,
    otp_app: :chatt_app,
    adapter: Ecto.Adapters.Postgres
end
