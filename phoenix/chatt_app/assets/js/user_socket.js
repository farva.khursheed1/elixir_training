// NOTE: The contents of this file will only be executed if
// you uncomment its entry in "assets/js/app.js".

// Bring in Phoenix channels client library:
import {Socket, Presence} from "phoenix"

// And connect to the path in "lib/chatt_app_web/endpoint.ex". We pass the
// token for authentication. Read below how it should be used.
let socket = new Socket("/socket", {params: {token: window.userToken}})

// When you connect, you'll often need to authenticate the client.
// For example, imagine you have an authentication plug, `MyAuth`,
// which authenticates the session and assigns a `:current_user`.
// If the current user exists you can assign the user's token in
// the connection for use in the layout.
//
// In your "lib/chatt_app_web/router.ex":
//
//     pipeline :browser do
//       ...
//       plug MyAuth
//       plug :put_user_token
//     end
//
//     defp put_user_token(conn, _) do
//       if current_user = conn.assigns[:current_user] do
//         token = Phoenix.Token.sign(conn, "user socket", current_user.id)
//         assign(conn, :user_token, token)
//       else
//         conn
//       end
//     end
//
// Now you need to pass this token to JavaScript. You can do so
// inside a script tag in "lib/chatt_app_web/templates/layout/app.html.heex":
//
//     <script>window.userToken = "<%= assigns[:user_token] %>";</script>
//
// You will need to verify the user token in the "connect/3" function
// in "lib/chatt_app_web/channels/user_socket.ex":
//
//     def connect(%{"token" => token}, socket, _connect_info) do
//       # max_age: 1209600 is equivalent to two weeks in seconds
//       case Phoenix.Token.verify(socket, "user socket", token, max_age: 1_209_600) do
//         {:ok, user_id} ->
//           {:ok, assign(socket, :user, user_id)}
//
//         {:error, reason} ->
//           :error
//       end
//     end
//
// Finally, connect to the socket:
let roomId = window.roomId
let presences = {}

socket.connect()

// Now that you are connected, you can join channels with a topic.
// Let's assume you have a channel with a topic named `room` and the
// subtopic is its id - in this case 42:
if (roomId){
  const timeout = 2000
  var typingTimer
  let userTyping = false

  let channel = socket.channel(`room:${roomId}`, {})
  channel.join()
    .receive("ok", resp => { 
      console.log("Joined successfully", resp) 
      resp.messages.reverse().map(msg => displayMessage(msg))
    })
    .receive("error", resp => { console.log("Unable to join", resp) })

  channel.on(`room:${roomId}:new_message`, (message) => {
    console.log(message)
    displayMessage(message)
  })

  channel.on("presence_state", state => {
    presences = Presence.syncState(presences, state)
    console.log(presences)
    displayUsers(presences)
  })

  channel.on("presence_diff", diff => {
    presences = Presence.syncDiff(presences, diff)
    console.log(presences)
    displayUsers(presences)
  })

  document.querySelector('#message-form').addEventListener('submit', (e) => {
    e.preventDefault()
    let input = e.target.querySelector('#message-body')

    channel.push('message:add', {
      message: input.value
    })

    input.value = ''
  })

  document.querySelector('#message-body').addEventListener('keydown', () => {
    userStartsTyping()
    clearTimeout(typingTimer)
  })
  document.querySelector('#message-body').addEventListener('keyup', () => {
    clearTimeout(typingTimer)
    typingTimer = setTimeout(userStopTyping, timeout)
  })

  const userStartsTyping = () => {
    if (userTyping) {
      return
    }

    userTyping = true
    channel.push('user:typing', {
      typing: true
    })
  }

  const userStopTyping = () => {
    clearTimeout(typingTimer)
    userTyping = false

    channel.push('user:typing', {
      typing: false
    })
  }

  const displayMessage = (msg) => {
    console.log('display message')
    let template = `
    <li class="list-group-item"><strong>${msg.user.username}</strong>: ${msg.body}</li>
  `

    document.querySelector('#display').innerHTML += template
  }

  const displayUsers = (presences) => {
    let usersOnline = Presence.list(presences, (_id, {
      metas: [
        user, ...rest
      ]
    }) => {
      var typingTemplate = ''
      if (user.typing) {
        typingTemplate = ' <i>(Typing...)</i>'
      }
      return `
        <div id="user-${user.user_id}"><strong class="text-secondary">${user.username}</strong> ${typingTemplate}</div>
      `
    }).join("")

    document.querySelector('#users-online').innerHTML = usersOnline
  }
}

export default socket
