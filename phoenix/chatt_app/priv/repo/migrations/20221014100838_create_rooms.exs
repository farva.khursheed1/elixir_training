defmodule ChattApp.Repo.Migrations.CreateRooms do
  use Ecto.Migration

  def up do
    create table(:rooms) do
      add :title, :string, null: false, size: 30
      add :topic, :string, size: 120
      add :description, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end
    create index(:posts, [:user_id])
    create unique_index(:rooms, [:title])
  end

  def down do
    drop index(:posts, [:user_id])
    drop unique_index(:rooms, [:title])
    drop table(:rooms)
  end
end
