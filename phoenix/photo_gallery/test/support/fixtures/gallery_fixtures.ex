defmodule PhotoGallery.GalleryFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `PhotoGallery.Gallery` context.
  """

  @doc """
  Generate a photo.
  """
  def photo_fixture(attrs \\ %{}) do
    {:ok, photo} =
      attrs
      |> Enum.into(%{
        photo: "some photo",
        uuid: "some uuid"
      })
      |> PhotoGallery.Gallery.create_photo()

    photo
  end
end
