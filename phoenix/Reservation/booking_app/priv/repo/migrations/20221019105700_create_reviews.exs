defmodule BookingApp.Repo.Migrations.CreateReviews do
  use Ecto.Migration

  def up do
    create table(:reviews) do
      add :rating, :integer
      add :comment, :string

      add :place_id, references(:places, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end
  end

  def down do
    drop table(:reviews)
  end
end
