defmodule BookingApp.Repo.Migrations.CreateBookings do
  use Ecto.Migration

  def up do
    create table(:bookings) do
      add :start_date, :date, null: false
      add :end_date, :date, null: false
      add :state, :string, null: false
      add :total_price, :decimal
      add :place_id, references(:places), null: false
      add :user_id, references(:users), null: false

      timestamps()
    end
    create index(:bookings, [:place_id, :user_id])
  end

  def down do
    drop index(:bookings, [:place_id, :user_id])
    drop table(:bookings)
  end
end
