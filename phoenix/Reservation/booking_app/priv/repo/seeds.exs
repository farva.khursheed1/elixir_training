# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BookingApp.Repo.insert!(%BookingApp.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias BookingApp.Repo
alias BookingApp.Vacation.{Place, Booking, Review}
alias BookingApp.Accounts.User

image_url = "#{BookingAppWeb.Endpoint.url()}/images"

farva =
  %User{
    email: "farvakh@gmail.com",
    hashed_password: "$2b$12$raoE8EjknGsZPu2M0Ywd7eXvToDh/Ohsux97dp9y7XhnEioRI4f3K"
} |> Repo.insert!

%Place{
  name: "Sand Castle",
  slug: "sand-castle",
  description: "Build endless sand castles in your front yard",
  location: "Portugal",
  max_guests: 2,
  pet_friendly: false,
  pool: false,
  wifi: false,
  price_per_night: Decimal.from_float(195.00),
  image: "#{image_url}/sand-castle.jpeg",
  image_thumbnail: "#{image_url}/sand-castle.jpeg",
  bookings: [
    %Booking{
      start_date: ~D[2022-10-18],
      end_date: ~D[2022-10-21],
      total_price: Decimal.from_float(585.00),
      user: farva
    }
  ]
} |> Repo.insert!

%Place{
  name: "Blue Igloo",
  slug: "blue-igloo",
  description: "Chill out",
  location: "Canada",
  max_guests: 3,
  pet_friendly: false,
  pool: false,
  wifi: false,
  price_per_night: Decimal.from_float(100.00),
  image: "#{image_url}/blue-igloo.jpeg",
  image_thumbnail: "#{image_url}/blue-igloo-thumb.jpeg",
  bookings: [
    %Booking{
      start_date: ~D[2022-10-21],
      end_date: ~D[2022-10-31],
      total_price: Decimal.from_float(1000.00),
      user: farva
    }
  ],
  reviews: [
    %Review{
      comment: "It's a chillaxing experience!",
      rating: 5,
      user: farva,
    }
  ]
} |> Repo.insert!

%Place{
  name: "Ski Cabin",
  slug: "ski-cabin",
  description: "Ski in and ski out!",
  location: "Switzerland",
  max_guests: 6,
  pet_friendly: true,
  pool: false,
  wifi: false,
  price_per_night: Decimal.from_float(225.00),
  image: "#{image_url}/ski-cabin.jpeg",
  image_thumbnail: "#{image_url}/ski-cabin.jpeg",
} |> Repo.insert!

%Place{
  name: "Hobbit House",
  slug: "hobbit-house",
  description: "Short cuts make delays, but inns make longer ones.",
  location: "New Zealand",
  max_guests: 4,
  pet_friendly: true,
  pool: false,
  wifi: true,
  price_per_night: Decimal.from_float(250.00),
  image: "#{image_url}/hobit-house.jpeg",
  image_thumbnail: "#{image_url}/hobit-house.jpeg",
} |> Repo.insert!
