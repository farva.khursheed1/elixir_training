defmodule BookingAppWeb.PageController do
  use BookingAppWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
