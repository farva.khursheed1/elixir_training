defmodule BookingAppWeb.Resolvers.Accounts do
  alias BookingApp.Accounts
  alias BookingAppWeb.Schema.ChangesetErrors


  def signup(_, args, _) do
    case Accounts.register_user(args) do
      {:error, changeset} ->
        {
          :error,
          message: "Could not create account!",
          details: ChangesetErrors.error_details(changeset)
        }
      {:ok, user} ->
        token = BookingAppWeb.AuthToken.sign(user)
        {:ok, %{user: user, token: token}}
    end
  end

end
