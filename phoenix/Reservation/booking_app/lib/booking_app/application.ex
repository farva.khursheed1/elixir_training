defmodule BookingApp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    import Supervisor.Spec
    children = [
      # Start the Ecto repository
      BookingApp.Repo,
      # Start the Telemetry supervisor
      BookingAppWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: BookingApp.PubSub},
      # Start the Endpoint (http/https)
      BookingAppWeb.Endpoint,
      supervisor(Absinthe.Subscription, [BookingAppWeb.Endpoint])
      # Start a worker by calling: BookingApp.Worker.start_link(arg)
      # {BookingApp.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BookingApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    BookingAppWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
