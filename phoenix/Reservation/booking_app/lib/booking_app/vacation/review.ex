defmodule BookingApp.Vacation.Review do
  use Ecto.Schema
  import Ecto.Changeset

  schema "reviews" do
    field :comment, :string
    field :rating, :integer

    belongs_to :place, BookingApp.Vacation.Place
    belongs_to :user, BookingApp.Accounts.User

    timestamps()
  end

  @doc false
  def changeset(review, attrs) do
    review
    |> cast(attrs, [:rating, :comment, :place_id])
    |> validate_required([:rating, :comment, :place_id])
    |> assoc_constraint(:place)
    |> assoc_constraint(:user)
  end
end
