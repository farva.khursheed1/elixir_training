defmodule BookingApp.Repo do
  use Ecto.Repo,
    otp_app: :booking_app,
    adapter: Ecto.Adapters.Postgres
end
