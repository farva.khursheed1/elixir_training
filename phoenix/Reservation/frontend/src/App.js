import {BrowserRouter, Route, Switch} from "react-router-dom";
import React from "react"
import Header from './components/Header';
import Footer from './components/Footer';
import Home from './pages/Home';
import Search from './pages/Search';
import Place from './pages/Place';
import MyBookings from './pages/MyBookings';
import Signup from './pages/Signup';
import Signin from './pages/Signin';
import NotFound from './pages/NotFound';

import './App.css';

function App() {
  <BrowserRouter>
      <div id="app">
        <Header />
        <div id="content">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/places" exact component={Search} />
            <Route path="/places/:slug" exact component={Place} />
            <Route path="/bookings" exact component={MyBookings} />
            <Route path="/sign-up" exact component={Signup} />
            <Route path="/sign-in" exact component={Signin} />
            <Route component={NotFound} />
          </Switch>
        </div>
        <Footer />
      </div>
  </BrowserRouter>
};

export default App;
