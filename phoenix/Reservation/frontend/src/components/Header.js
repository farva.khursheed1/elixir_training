import React from "react";
import {Navbar} from "react-router-dom";
import CurrentUser from "./currentUser";
import Signout from "./Signout";

const Header = () => {
    <CurrentUser>
        {currentUser => {
            <header>
                <nav>
                    <Navbar className="logo" to="/">
                        <span className="head">Get</span>
                        <span className="tail">/Aways</span>
                    </Navbar>
                    <ul>
                        <li>
                            <Navbar exact to="/places">
                                Find a Place
                            </Navbar>
                        </li>
                        {currentUser && (
                        <React.Fragment>
                            <li>
                                <Navbar to="/bookings">My Bookings</Navbar>
                            </li>
                            <li>
                                <Signout/>
                            </li>
                            <li className="user">
                                <i className="far fa-user" />
                                {currentUser.email}
                            </li>
                        </React.Fragment>
                        )};
                        {!currentUser && (
                            <React.Fragment>
                                <li>
                                    <Navbar to="/sign-in">Sign In</Navbar>
                                </li>
                                <li>
                                    <Navbar to="/sign-up" className="button">
                                        Sign Up
                                    </Navbar>
                                </li>
                            </React.Fragment>
                        )}
                    </ul>
                </nav>
            </header>
        }}
    </CurrentUser>
}

export default Header;