import React from "react";
import PropTypes from "prop-types";

const Error = ({error}) => {
    if (!error || !error.message) return null;

    const isNetworkError = 
        error.networkError &&
        error.networkError.message &&
        error.networkError.statusCode;

    const hasGraphQLErrors = error.graphQLErrors && error.graphQLErrors.length;

    let errorMessage;

    if (isNetworkError){
        if (error.networkError.statusCode === 404){
            errorMessage = (
                <h3>
                    <code>404: Not Found</code>
                </h3>
            );
        }else{
            errorMessage = (
                <React.Fragment>
                <h3>Network Error!</h3>
                <code>
                    {error.networkError.statusCode}: {error.networkError.message}
                </code>
                </React.Fragment>
            );
        }
    }else if (hasGraphQLErrors) {
        errorMessage = (
            <React.Fragment>
            <ul>
                {error.graphQLErrors.map(({message, details}, i) => {
                    <li key={i}>
                        <span className="message">{message}</span>
                        {details && (
                            <ul>
                                {Object.keys(details).map(key => (
                                <li key={key}>
                                    {key} {details[key]}
                                </li>
                            ))}
                            </ul>
                        )}
                    </li>
                })}
            </ul>
            </React.Fragment>
        )
    }
}

export default Error;