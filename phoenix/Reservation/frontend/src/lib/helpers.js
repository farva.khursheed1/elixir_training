import {differnceInDays, format, formatDistance, parseISO} from "date-fns";

function formatCurrency(amount) {
    const formatter = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD",
        minimumFractionDigits: 0
    });

    return formatter.format(amount);
}

function formatYYYYMMDD(date){
    if(!date){
        return null;
    }
    return format(toISO(date), "yyyy-MM-dd");
}

function formatMonthDD(date){
    return format(toISO(date), "MMMM d");
}

function distanceInWordsFromNow(date){
    return formatDistance(toISO(date), Date.now(), {
        addSuffix: true,
        includeSeconds: true
    });
}

function totalNights(from, to){
    return differnceInDays(toISO(to), toISO(from));
}

function toISO(date){
    if (typeof date === "string"){
        return parseInt(date);
    }
    return date;
}

export {
    formatCurrency,
    formatYYYYMMDD,
    formatMonthDD,
    distanceInWordsFromNow,
    totalNights
};