defmodule HelloWeb.PageController do
  use HelloWeb, :controller
  def index(conn, _params) do
    render(conn, "index.html")
  end
  # def show(conn, _params) do
  #   page = %{tite: "foo"}
  #   render(conn, "show.json", page: page)
  # end

  # def index(conn, _params) do
  #   page = [%{tite: "foo"}, %{title: "bar"}]
  #   render(conn, "show.json", page: page)
  # end
end
