defmodule HelloWeb.HelloController do
    use HelloWeb, :controller
  
    def index(conn, _params) do
      conn
      |> put_flash(:info, "Welcome to Phoenix, from flash info!")
      |> put_flash(:error, "Let's pretend we have an error")
      |> redirect(to: Routes.hello_path(conn, :redirect_test))
    end

    def redirect_test(conn, _params) do
      render(conn, "index.html")
    end

    def show(conn, %{"messenger" => messenger}) do
      render(conn, "show.html", messenger: messenger)
    end
  end
  