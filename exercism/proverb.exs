defmodule Proverb do
  @doc """
  Generate a proverb from a list of strings.
  """
  @spec recite(strings :: [String.t()]) :: String.t()
  def recite([]), do:  ""
  def recite([things]) do
    """
    And all for the want of a #{things}.
    """
  end
  def recite([things | rest] = strings) do
    lines = 
      Enum.zip(strings, rest)
      |> Enum.map_join("\n", fn {a, b} -> "For want of a #{a} the #{b} was lost." end)

    """
    #{lines}
    And all for the want of a #{things}.
    """
  end
end
