defmodule Secrets do
    def secret_add(secret) do
      fn(a) -> a + secret  end
      # Please implement the secret_add/1 function
    end
  
    def secret_subtract(secret) do
      fn(a) -> a - secret  end
      # Please implement the secret_subtract/1 function
    end
  
    def secret_multiply(secret) do
      fn(a) -> a * secret  end
      # Please implement the secret_multiply/1 function
    end
  
    def secret_divide(secret) do
      fn(a) -> div(a, secret)  end
      # Please implement the secret_divide/1 function
    end
  
    def secret_and(secret) do
      use Bitwise, only_operators: true
      fn(a) -> a &&& secret  end
      # Please implement the secret_and/1 function
    end
  
    def secret_xor(secret) do
      use Bitwise
      fn(a) -> bxor(a, secret) end
      # Please implement the secret_xor/1 function
    end
  
    def secret_combine(secret_function1, secret_function2) do
      fn x -> x |> secret_function1.() |> secret_function2.() end
      # Please implement the secret_combine/2 function
    end
  end
  