defmodule AllYourBase do
    @moduledoc """
    Convert a number, represented as a sequence of digits in one base, to any other base.
    """
    @error_message "all digits must be >= 0 and < input base"
    @input_base_error "input base must be >= 2"
    @output_base_error "output base must be >= 2"
    @doc false
    @spec convert(list(non_neg_integer), pos_integer, pos_integer) ::
            {:ok | :error, list(non_neg_integer) | String.t()}
    def convert(_, _a, b) when b < 2, do: {:error, @output_base_error}
    def convert(_, a, _b) when a < 2, do: {:error, @input_base_error}
    def convert(digits, base_a, base_b) do
      if invalid?(digits, base_a) do
        {:error, @error_message}
      else
        result =
          digits
          |> Enum.reverse()
          |> Enum.with_index()
          |> Enum.map(fn {k, i} ->
            (k * :math.pow(base_a, i)) |> trunc()
          end)
          |> Enum.sum()
          |> to_base(base_b)
        {:ok, result}
      end
    end
    # check if given digits are invalid
    defp invalid?(digits, base) do
      Enum.any?(digits, &(&1 < 0 || &1 >= base))
    end
    # convert `a` in base `b`
    defp to_base(0, _), do: [0]
    defp to_base(a, b), do: to_base(a, b, [])
    defp to_base(0, _, result), do: result
    defp to_base(a, b, result), do: to_base(div(a, b), b, [rem(a, b) | result])
  end