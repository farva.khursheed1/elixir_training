defmodule FileSniffer do
    def type_from_extension(extension) do
      case extension do
        "exe" -> "application/octet-stream"
        "bmp" -> "image/bmp"
        "png" -> "image/png"
        "jpg" -> "image/jpg"
        "gif" -> "image/gif"
        _ -> nil
      end
    end
  
    def type_from_binary(file_binary) do
      <<head :: binary-size(8), body :: binary>> = file_binary
      case head do
        <<0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A>> -> "image/png"
        <<0x7F, 0x45, 0x4C, 0x46, _::binary>> -> "application/octet-stream"
        <<0xFF, 0xD8, 0xFF, _::binary>> -> "image/jpg"
        <<0x47, 0x49, 0x46, _::binary>> -> "image/gif"
        <<0x42, 0x4D, _::binary>> -> "image/bmp"
        _ -> nil 
      end
    end
  
    def verify(file_binary, extension) do
      binary = type_from_binary(file_binary)
      exten = type_from_extension(extension)
      if binary == exten do
          {:ok, binary}
      else
        {:error, "Warning, file format and file extension do not match."}
      end
    end
  end
  