defmodule IsbnVerifier do
    @doc """
      Checks if a string is a valid ISBN-10 identifier
  
      ## Examples
  
        iex> IsbnVerifier.isbn?("3-598-21507-X")
        true
  
        iex> IsbnVerifier.isbn?("3-598-2K507-0")
        false
  
    """
    @spec isbn?(String.t()) :: boolean
    def isbn?(isbn) when is_binary(isbn), do: isbn?(isbn |> String.to_charlist())
    def isbn?(isbn) when is_list(isbn) do
      isbn = isbn |> Enum.reject(&(&1 === ?-))
      cond do
        length(isbn) !== 10 ->
          false
        not (isbn |> Enum.take(9) |> Enum.all?(&(&1 in '0123456789'))) ->
          false
        not ((isbn |> Enum.at(9)) in '0123456789X') ->
          false
        true ->
          isbn
          |> Enum.map(fn
            ?X -> 10
            x -> List.to_string([x]) |> String.to_integer()
          end)
          |> Enum.reverse()
          |> Enum.with_index(1)
          |> Enum.reverse()
          |> Enum.reduce(0, fn {x, i}, acc -> acc + x * i end)
          |> Integer.mod(11) === 0
      end
    end
  end
  