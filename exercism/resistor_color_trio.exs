defmodule ResistorColorTrio do
    @doc """
    Calculate the resistance value in ohm or kiloohm from resistor colors
    """
    @colors [
        black: 0,
        brown: 1,
        red: 2,
        orange: 3,
        yellow: 4,
        green: 5,
        blue: 6,
        violet: 7,
        grey: 8,
        white: 9
      ]
  
    @spec label(colors :: [atom]) :: {number, :ohms | :kiloohms}
    def label([color1, color2, color3]) do
      value = (@colors[color1] * 10 + @colors[color2]) * :math.pow(10, @colors[color3])
      cond do
        value > 1000 -> {value / 1000, :kiloohms}
        true -> {value, :ohms}
      end
    end
  end
  