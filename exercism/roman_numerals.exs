defmodule RomanNumerals do
    @doc """
    Convert the number to a roman number.
    """
    @spec numeral(pos_integer) :: String.t()
    def numeral(num) do
      check(num)
    end
    defp check(number) when number >= 1000, do: "M" <> check(number - 1000)
  
    defp check(number) when number >= 900,  do: "CM" <> check(number - 900)
    defp check(number) when number >= 800,  do: "DCCC" <> check(number - 800)
    defp check(number) when number >= 700,  do: "DCC" <> check(number - 700)
    defp check(number) when number >= 600,  do: "DC" <> check(number - 600)
    defp check(number) when number >= 500,  do: "D" <> check(number - 500)
    defp check(number) when number >= 400,  do: "CD" <> check(number - 400)
    defp check(number) when number >= 300,  do: "CCC" <> check(number - 300)
    defp check(number) when number >= 200,  do: "CC" <> check(number - 200)
    defp check(number) when number >= 100,  do: "C" <> check(number - 100)
  
    defp check(number) when number >= 90,   do: "XC" <> check(number - 90)
    defp check(number) when number >= 80,   do: "LXXX" <> check(number - 80)
    defp check(number) when number >= 70,   do: "LXX" <> check(number - 70)
    defp check(number) when number >= 60,   do: "LX" <> check(number - 60)
    defp check(number) when number >= 50,   do: "L" <> check(number - 50)
    defp check(number) when number >= 40,   do: "XL" <> check(number - 40)
    defp check(number) when number >= 30,   do: "XXX" <> check(number - 30)
    defp check(number) when number >= 20,   do: "XX" <> check(number - 20)
    defp check(number) when number >= 10,   do: "X" <> check(number - 10)
  
    defp check(number) when number == 9,   do: "IX" <> check(number - 9)
    defp check(number) when number == 8,   do: "VIII" <> check(number - 8)
    defp check(number) when number == 7,   do: "VII" <> check(number - 7)
    defp check(number) when number == 6,   do: "VI" <> check(number - 6)
    defp check(number) when number == 5,   do: "V" <> check(number - 5)
    defp check(number) when number == 4,   do: "IV" <> check(number - 4)
    defp check(number) when number == 3,   do: "III" <> check(number - 3)
    defp check(number) when number == 2,   do: "II" <> check(number - 2)
    defp check(number) when number == 1,   do: "I" <> check(number - 1)
  
    defp check(number), do: ""
  end
  