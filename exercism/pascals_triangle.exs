defmodule PascalsTriangle do
    @doc """
    Calculates the rows of a pascal triangle
    with the given height
    """
    @spec rows(integer) :: [[integer]]
    def rows(num) do
      Enum.map(
        0..num-1,
        fn c ->
          combinations(c)
        end
      )
    end
    defp factorial(0), do: 1
    defp factorial(n) when n > 0, do: n * factorial(n - 1)
    defp combination(n, r), do: trunc(factorial(n) / (factorial(r) * factorial(n - r)))
    defp combinations(c), do: Enum.map(0..c, &combination(c, &1))
  end
  