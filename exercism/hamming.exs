defmodule Hamming do
    @doc """
    Returns number of differences between two strands of DNA, known as the Hamming Distance.
  
    ## Examples
  
    iex> Hamming.hamming_distance('AAGTCATA', 'TAGCGATC')
    {:ok, 4}
    """
    @length_error "strands must be of equal length"
    @spec hamming_distance([char], [char]) :: {:ok, non_neg_integer} | {:error, String.t()}
    def hamming_distance(strand1, strand2) do
      if Enum.count(strand1) == Enum.count(strand2) do
          count = 
          Enum.zip(strand1, strand2)
          |> Enum.count(&(not equal?(&1)))
        {:ok, count}
      else
          {:error, @length_error}
      end
    end
  
    defp equal?({n, n}), do: true
    defp equal?({_n, _m}), do: false
  end
  