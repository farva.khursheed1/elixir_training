defmodule Chessboard do
    def rank_range do
      1..8
    end
  
    def file_range do
      65..72
    end
  
    def ranks do
      rank_range()
      |> Enum.to_list()
    end
  
    def files do
      file_range()
      |> Enum.to_list()
      |> List.to_string()
      |> String.graphemes()
    end
  end
  