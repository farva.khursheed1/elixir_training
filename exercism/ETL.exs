object Etl
{
  def transform(scores: Map[Int, Seq[String]]): Map[String, Int] = {
    for {
      (score, strings) <- scores
      string <- strings  
    } yield (string.toLowerCase, score)
  }
}