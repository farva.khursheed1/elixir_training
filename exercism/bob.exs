defmodule Bob do
    @spec hey(String.t()) :: String.t()
    def hey(input) do
      cond do
        shouting?(input) && asking_question?(input) && !contatins_number?(input) -> "Calm down, I know what I'm doing!"
        shouting?(input) && !silence?(input) && !contains_only_number?(input) -> "Whoa, chill out!"
        asking_question?(input) -> "Sure."
        silence?(input) -> "Fine. Be that way!"
        true -> "Whatever."
      end
    end
    defp shouting?(input) do
      input == String.upcase(input)
    end
    defp asking_question?(input) do
      String.ends_with?(input, "?")
    end
    defp silence?(input) do
      String.trim(input) == ""
    end
    defp contatins_number?(input) do
      Regex.match?(~r/[0-9]/, input)
    end
    defp contatins_letters?(input) do
      Regex.match?(~r/[a-z]/, input)
    end
    defp contains_only_number?(input) do
      String.trim(String.replace(input, ~r/([0-9]|,|\?)/, "")) == ""
    end
  end