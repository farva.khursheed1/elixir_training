defmodule Isogram do
    @doc """
    Determines if a word or sentence is an isogram
    """
    @spec isogram?(String.t()) :: boolean
    def isogram?(sentence) do
      string_split =
        sentence
        |> String.replace(~r/[- ]/, "")
        |> String.downcase()
        |> String.split("", trim: true)
      string_split_unique = Enum.uniq(string_split)
      Kernel.length(string_split) == Kernel.length(string_split_unique)
    end
  end
  