defmodule ArmstrongNumber do
    @moduledoc """
    Provides a way to validate whether or not a number is an Armstrong number
    """
  
    @spec valid?(integer) :: boolean
    def valid?(0), do: true
    def valid?(number) do
      digits = Integer.digits(number)
      count = length(digits)
      result =
          digits
          |> Enum.map(&:math.pow(&1,count))
          |> Enum.sum()
      cond do
        number == result -> true
        true -> false
      end
    end
  end
  