defmodule Triangle do
    @type kind :: :equilateral | :isosceles | :scalene
  
    @doc """
    Return the kind of triangle of a triangle with 'a', 'b' and 'c' as lengths.
    """
    @spec kind(number, number, number) :: {:ok, kind} | {:error, String.t()}
    def kind(a, b, c) do
      cond do
        not all_side_lengths_positive?(a, b, c) ->
          {:error, "all side lengths must be positive"}
    
        not triangle_meets_triangle_inequality?(a, b, c) ->
          {:error, "side lengths violate triangle inequality"}
    
        is_equilateral?(a, b, c) ->
          {:ok, :equilateral}
    
        is_isosceles?(a, b, c) ->
          {:ok, :isosceles}
    
        is_scalene?(a, b, c) ->
          {:ok, :scalene}
    
        true ->
          {:error, "Not a valid triangle"}
      end
    end
  
    defp is_equilateral?(a, b, c) do
      a == b and b == c
    end
    
    defp is_isosceles?(a, b, c) do
      a == b or a == c or b == c
    end
    
    defp is_scalene?(a, b, c) do
      a != b and a != c and b != c
    end
  
    defp all_side_lengths_positive?(a, b, c) do
      a > 0 and b > 0 and c > 0
    end
  
    defp triangle_meets_triangle_inequality?(a, b, c) do
      b + c > a and a + c > b and a + b > c
    end
  end
  