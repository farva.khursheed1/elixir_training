defmodule Grains do
    @doc """
    Calculate two to the power of the input minus one.
    """
    @spec square(pos_integer()) :: {:ok, pos_integer()} | {:error, String.t()}
    def square(number) do
      if number > 0 and number <= 64 do
        {:ok, :math.pow(2, number - 1) |> trunc()}
      else
        {:error, "The requested square must be between 1 and 64 (inclusive)"}
      end
    end
  
    @doc """
    Adds square of each number from 1 to 64.
    """
    @spec total :: {:ok, pos_integer()}
    def total do
      {:ok, Enum.reduce(1..64, 0, fn n, acc -> acc + elem(square(n), 1) end)}
    end
  end
  