defmodule Allergies do
    @doc """
    List the allergies for which the corresponding flag bit is true.
    """
    use Bitwise
    @allerges [
      {"eggs", 1},
      {"peanuts", 2},
      {"shellfish", 4},
      {"strawberries", 8},
      {"tomatoes", 16},
      {"chocolate", 32},
      {"pollen", 64}, 
      {"cats", 128}
    ]
    @spec list(non_neg_integer) :: [String.t()]
    def list(flags) do
      @allerges
      |> Enum.filter(fn{_, score} -> (flags &&& score) > 0 end)
      |> Enum.map(fn{key, _} -> key end) 
    end
  
    @doc """
    Returns whether the corresponding flag bit in 'flags' is set for the item.
    """
    @spec allergic_to?(non_neg_integer, String.t()) :: boolean
    def allergic_to?(flags, item) do
      list(flags)
      |> Enum.find(fn a -> a==item end)
      |> Kernel.!=(nil)
    end
  end
  