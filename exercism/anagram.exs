defmodule Anagram do
    @doc """
    Returns all candidates that are anagrams of, but not equal to, 'base'.
    """
    @spec match(String.t(), [String.t()]) :: [String.t()]
    def match(base, candidates) do
      for word <- candidates, anagram?(base, word), do: word
    end
  
    def anagram?(word_1, word_2) do
      cond do
        String.downcase(word_1) == String.downcase(word_2) -> false
        true -> word_to_list(word_1) == word_to_list(word_2)
      end
    end
  
    def word_to_list(word) do
      word |> String.downcase() |> String.graphemes() |> Enum.sort 
    end
  
  end
  