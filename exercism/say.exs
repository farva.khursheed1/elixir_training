defmodule Say do
  @doc """
  Translate a positive integer into English.
  """
  @spec in_english(integer) :: {atom, String.t()}
  def in_english(number)
  def in_english(0), do: {:ok, "zero"}
  def in_english(number) when number < 0 or number > 999_999_999_999, do: {:error, "number is out of range"}
  def in_english(number) do
    {:ok, say(number)}
  end
  defp say(0), do: ""
  defp say(1), do: "one"
  defp say(2), do: "two"
  defp say(3), do: "three"
  defp say(4), do: "four"
  defp say(5), do: "five"
  defp say(6), do: "six"
  defp say(7), do: "seven"
  defp say(8), do: "eight"
  defp say(9), do: "nine"
  defp say(10), do: "ten"
  defp say(11), do: "eleven"
  defp say(12), do: "twelve"
  defp say(13), do: "thirteen"
  defp say(14), do: "fourteen"
  defp say(15), do: "fifteen"
  defp say(number) when number > 15 and number < 20, do: say(number-10) <> "teen"
  defp say(20), do: "twenty"
  defp say(30), do: "thirty"
  defp say(40), do: "forty"
  defp say(50), do: "fifty"
  defp say(60), do: "sixty"
  defp say(70), do: "seventy"
  defp say(80), do: "eighty"
  defp say(90), do: "ninety"
  defp say(number) when number > 20 and number < 100, do: say(div(number, 10) * 10) <> "-" <> say(rem(number, 10))
  defp say(number) when number > 99 and number < 1000, do: say(div(number, 100)) <> " hundred " <> say(rem(number, 100)) |> String.trim
  defp say(number) when number > 999 and number < 1000000, do: say(div(number, 1000)) <> " thousand " <> say(rem(number, 1000)) |> String.trim
  defp say(number) when number > 999999 and number < 1000000000, do: say(div(number, 1000000)) <> " million " <> say(rem(number, 1000000)) |> String.trim
  defp say(number), do: say(div(number, 1000000000)) <> " billion " <> say(rem(number, 1000000000)) |> String.trim
end