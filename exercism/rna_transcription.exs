defmodule RnaTranscription do
    @doc """
    Transcribes a character list representing DNA nucleotides to RNA
  
    ## Examples
  
    iex> RnaTranscription.to_rna('ACTG')
    'UGAC'
    """
    @dna_to_rna %{71=>67,67=>71,84=>65,65=>85}
    @spec to_rna([char]) :: [char]
    def to_rna(dna) do
      dna
      |> Enum.map(&rna/1)
    end
  
    def rna(dna_nuc) do
      @dna_to_rna[dna_nuc]
    end
  end
  