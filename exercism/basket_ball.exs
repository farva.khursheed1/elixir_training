defmodule BasketballWebsite do
    def extract_from_path(data, path) do
      split = String.split(path, ".")
      extract_path(data, split)
    end
    defp extract_path(data, []), do: nil
    defp extract_path(data, [path]), do: data[path]
    defp extract_path(data, [head | tail]), do: extract_path(data[head], tail)
  
    def get_in_path(data, path) do
      split = String.split(path, ".")
      get_in(data, split)
    end
  end
  