defmodule LibraryFees do
    def datetime_from_string(string) do
      NaiveDateTime.from_iso8601!(string)
    end
  
    def before_noon?(datetime) do
      cond do
        datetime.hour >= 12 -> false
        true -> true
      end
    end
  
    def return_date(checkout_datetime) do
      days_to_add = case before_noon?(checkout_datetime) do
         true ->28
         false -> 29    
        end
      checkout_datetime |> NaiveDateTime.to_date() |> Date.add(days_to_add)
    end
  
    def days_late(planned_return_date, actual_return_datetime) do
      late_days = Date.diff(NaiveDateTime.to_date(actual_return_datetime), planned_return_date)
      if late_days <= 0 do
      0
      else
      late_days
      end
    end
  
    def monday?(datetime) do
      datetime
      |> NaiveDateTime.to_date()
      |> Date.day_of_week()
      |> Kernel.==(1)
    end
  
    def calculate_late_fee(checkout, return, rate) do
      actual_return = datetime_from_string(return)
      days_late = checkout |> datetime_from_string() |> return_date() |> days_late( actual_return)
      rate = if monday?(actual_return), do: rate * 0.5, else: rate
      trunc(days_late * rate)
    end
  end
  