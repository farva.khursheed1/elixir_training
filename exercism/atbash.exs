defmodule Atbash do
  @doc """
  Encode a given plaintext to the corresponding ciphertext

  ## Examples

  iex> Atbash.encode("completely insecure")
  "xlnko vgvob rmhvx fiv"
  """
  @spec encode(String.t()) :: String.t()
  def encode(plaintext) do
    plaintext
    |> String.downcase
    |> String.to_charlist
    |> Enum.filter(&valid_chars?/1)
    |> Enum.chunk_every(5)
    |> Enum.map_join(" ", fn chunk -> chunk |> Enum.map(&map_value/1) end)
  end
  defp valid_chars?(c), do: c in ?a..?z or c in ?0..?9
  defp map_value(l) when l in ?a..?z, do: (?a + ?z - l)
  defp map_value(x), do: x

  @spec decode(String.t()) :: String.t()
  def decode(cipher) do
    cipher 
    |> String.to_charlist()
    |> Enum.filter(&valid_chars?/1)
    |> Enum.map(&map_value/1)
    |> to_string()
  end
end
