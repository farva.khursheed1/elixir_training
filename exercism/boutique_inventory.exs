defmodule BoutiqueInventory do
    def sort_by_price(inventory) do
      inventory
      |> Enum.sort_by(&(&1.price))
    end
  
    def with_missing_price(inventory) do
      inventory
      |> Enum.filter(&(&1.price) == nil)
    end
  
    def update_names(inventory, old_word, new_word) do
      Enum.map(inventory, fn item ->
      %{item | name: String.replace(item.name, old_word, new_word)} end)
    end
  
    def increase_quantity(item, count) do
      quantity_by_size = Map.new(item.quantity_by_size, fn{size, number} -> {size, number + count} end)
      %{item | quantity_by_size: quantity_by_size}
    end
  
    def total_quantity(item) do
      Enum.reduce(item.quantity_by_size, 0, fn{_size, number}, acc -> number + acc end)
    end
  end
  