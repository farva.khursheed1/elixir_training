defmodule KitchenCalculator do
    @cup 240
    @fluid_ounce 30
    @teaspoon 5
    @tablespoon 15
    @milliliter 1
    def get_volume({unit, volume}), do: volume 
      # Please implement the get_volume/1 function
  
    def to_milliliter({unit, amount}) do
      # Please implement the to_milliliter/1 functions
      case unit do
        :cup -> {:milliliter, amount * @cup}
        :fluid_ounce -> {:milliliter, amount * @fluid_ounce}
        :teaspoon -> {:milliliter, amount * @teaspoon}
        :tablespoon -> {:milliliter, amount * @tablespoon}
        :milliliter -> {:milliliter, amount * @milliliter}
      end
    end
  
    def from_milliliter({_, amount}, unit) do
      # Please implement the from_milliliter/2 functions
      case unit do
        :cup -> {:cup, amount / @cup}
        :fluid_ounce -> {:fluid_ounce, amount / @fluid_ounce}
        :teaspoon -> {:teaspoon, amount / @teaspoon}
        :tablespoon -> {:tablespoon, amount / @tablespoon} 
        :milliliter -> {:milliliter, amount / @milliliter}
      end
    end
  
    def convert(volume_pair, unit) do
      # Please implement the convert/2 function
      to_milliliter(volume_pair) |> from_milliliter(unit)
    end
  end
  