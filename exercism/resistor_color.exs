defmodule ResistorColor do
    @doc """
    Return the value of a color band
    """
    @colors [
        :black,
        :brown,
        :red,
        :orange,
        :yellow,
        :green,
        :blue,
        :violet,
        :grey,
        :white
    ]
    @spec code(String.t()) :: integer()
    def code(color), do: @colors |> Enum.find_index(&(&1 == color))
  end
  