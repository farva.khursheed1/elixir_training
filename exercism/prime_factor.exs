defmodule PrimeFactors do
  @doc """
  Compute the prime factors for 'number'.
 
  The prime factors are prime numbers that when multiplied give the desired
  number.
 
  The prime factors of 'number' will be ordered lowest to highest.
  """
  @spec factors_for(pos_integer) :: [pos_integer]
  def factors_for(1), do: []

  def factors_for(number) do
    factorize(number)
  end

  def factorize(n, m \\ 2, factors \\ [])

  def factorize(n, 2, factors) when n > 2 do
    if rem(n, 2) == 0 do
      factorize(div(n, 2), 2, [2 | factors])
    else
      factorize(n, 3, factors)
    end
  end

  def factorize(n, m, factors) when m < n do
    if rem(n, m) == 0 do
      factorize(div(n, m), m, [m | factors])
    else
      factorize(n, m + 2, factors)
    end
  end
  def factorize(n, _, factors), do: Enum.reverse([n | factors])
end